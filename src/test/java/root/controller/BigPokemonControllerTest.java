package root.controller;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import root.mapper.ResponceEntityToEntityResourceMapper;
import root.resources.entity.BigPokemonResource;

@ExtendWith(SpringExtension.class)
@Import({BigPokemonController.class, BigPokemonResource.class})
class BigPokemonControllerTest {

    @Autowired
    BigPokemonResource bigPokemonResource;

    @Test
    void drawBigPokemon() {
        BigPokemonResource bigPokemonResource = new ResponceEntityToEntityResourceMapper().mapper(1);
        String actualParameters = String.valueOf(bigPokemonResource.getPokemonId()) + bigPokemonResource.getName();
        Assert.assertEquals("1bulbasaur",actualParameters);
    }
}