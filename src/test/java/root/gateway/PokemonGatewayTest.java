package root.gateway;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@Import(PokemonGateway.class)
class PokemonGatewayTest {

    @Autowired
    private PokemonGateway pokemonGateway;

    @Test
    public void getPokemonList() {
        Object x = pokemonGateway.getPokemonList(0).getBody().getResults().isEmpty();
        assertEquals(false, x);
    }

    @Test
    public void getBigPokemon() throws NumberFormatException {
        try {
            pokemonGateway.getBigPokemon(Integer.parseInt("asdf"));
            Assert.fail("asdf");
        } catch (NumberFormatException e) {
            Assert.assertNotEquals("", NumberFormatException.class);
        }
    }
}