package root.resources.entity;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.*;
import org.springframework.hateoas.ResourceSupport;

@Data
public class BigPokemonResource extends ResourceSupport {

    private int pokemonId;
    private String name;
    private JsonNode abilities;
}
