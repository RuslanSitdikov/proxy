//Нужен для hateoas'а - цеплять линки
package root.resources.entity;

import lombok.Data;
import java.util.List;

@Data
public class BigPokemonList {
    private List<BigPokemon> bigPokemonList;

    public BigPokemonList(List<BigPokemon> content) {
        bigPokemonList = content;
    }
}