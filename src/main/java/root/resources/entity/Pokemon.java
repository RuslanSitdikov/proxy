package root.resources.entity;

import lombok.*;
import javax.persistence.*;

@Data
@Entity
@Table(name = "pokemons", schema = "public")
public class Pokemon {

    @Id//Переменную id заменил на pokemonId - для hateoas'a.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int pokemonId;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;

}