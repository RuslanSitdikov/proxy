package root.resources.entity;

import lombok.Data;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import root.deserializator.BigPokemonDeserializator;

import javax.persistence.*;

@Data
@Entity
@JsonDeserialize(using = BigPokemonDeserializator.class)
@Table(name = "bigpokemons", schema = "public")
public class BigPokemon  {

    @Id//Переменную id заменил на bigPokemonId - для hateoas'a.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int bigPokemonId;
    private String name;

    @Transient //пока в БД не пишем, из БД не таскаем)
    private JsonNode abilities;
}