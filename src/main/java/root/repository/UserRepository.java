package root.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import root.resources.entity.User;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    @Transactional//(propagation = Propagation.NESTED)
    @Query(value = "SELECT * FROM users WHERE name = 'qw1'", nativeQuery = true)
    User getQWUser();
}
