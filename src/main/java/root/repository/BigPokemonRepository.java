package root.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import root.resources.entity.BigPokemon;
import java.util.Optional;

public interface BigPokemonRepository extends PagingAndSortingRepository<BigPokemon, Integer> {

    @Query(value = "SELECT * FROM bigpokemons WHERE name = 'Bulbazavr'", nativeQuery = true)
    Optional<BigPokemon> getBulbazavr();

    @Query(value = "INSERT INTO bigpokemons VALUES (11, 'Bulbazavr2')", nativeQuery = true)
    void setBulbazavr2();




}