package root.repository;

import org.springframework.stereotype.Component;
import javax.persistence.*;

@Component
public class CustomFunnyRepository implements ICustomFunnyRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Object getBulbazavr() {
        try {
            return entityManager.createNativeQuery("SELECT * FROM bigpokemons WHERE name = 'Bulbazavr'");
        } catch (NullPointerException e) {
            return "Bulbazavr was not found";
        }
    }
}