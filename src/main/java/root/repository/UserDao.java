package root.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import root.resources.entity.User;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {
    User findByName(String name);
}