package root.repository;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ICustomFunnyRepository {
    public Object getBulbazavr();
}
