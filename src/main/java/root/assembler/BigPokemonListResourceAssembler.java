package root.assembler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import root.controller.BigPokemonPaginationControllers;
import root.repository.BigPokemonRepository;
import root.resources.entity.BigPokemonList;
import root.resources.entity.BigPokemonListResource;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class BigPokemonListResourceAssembler extends ResourceAssemblerSupport<BigPokemonList, BigPokemonListResource> {

    @Autowired
    BigPokemonRepository bigPokemonRepository;

    int pageNumer = 0;
    int pageQuantity = 0;

    public BigPokemonListResourceAssembler(int pageNumber, int pageQuantity) {
        super(BigPokemonPaginationControllers.class, BigPokemonListResource.class);
        this.pageNumer = pageNumber;
        this.pageQuantity = pageQuantity;
    }

    @Override
    public BigPokemonListResource toResource(BigPokemonList bigPokemonList) {
        BigPokemonListResource listResource = super.instantiateResource(bigPokemonList);
        listResource.setBigPokemonList(bigPokemonList);
        listResource.add(getLinks());
        return listResource;
    }

    private List<Link> getLinks() {
        List<Link> links = new ArrayList<>();

        if (this.pageNumer > 0) {
            links.add(linkTo(BigPokemonPaginationControllers.class).slash("previous").withRel("Previous"));
        }
        if (this.pageNumer < this.pageQuantity - 1) {
            links.add(linkTo(BigPokemonPaginationControllers.class).slash("next").withRel("Next"));
        }
        return links;
    }

}