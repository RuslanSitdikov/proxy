package root.assembler;

import com.google.common.collect.Lists;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;
import root.controller.BigPokemonController;
import root.resources.entity.BigPokemon;
import root.resources.entity.BigPokemonResource;

import java.util.List;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Component
public class BigPokemonResourceAssembler extends ResourceAssemblerSupport<BigPokemon, BigPokemonResource> {

    public BigPokemonResourceAssembler() {
        super(BigPokemonController.class, BigPokemonResource.class);
    }

    @Override
    public BigPokemonResource toResource(BigPokemon bigPokemon) {
        BigPokemonResource resource = super.instantiateResource(bigPokemon);
        resource.setPokemonId(bigPokemon.getBigPokemonId());
        resource.setName(bigPokemon.getName());
        resource.setAbilities(bigPokemon.getAbilities());
        resource.add(getLinks(bigPokemon));
        return resource;
    }

    private List<Link> getLinks(BigPokemon bigPokemon) {
        return Lists.newArrayList(
                linkTo(BigPokemonController.class).slash("next").slash(bigPokemon.getBigPokemonId()).withRel("Next"),
                linkTo(BigPokemonController.class).slash("previous").slash(bigPokemon.getBigPokemonId()).withRel("Previous"),
                linkTo(BigPokemonController.class).slash("pageableview").withRel("PageableView")
        );
    }
}