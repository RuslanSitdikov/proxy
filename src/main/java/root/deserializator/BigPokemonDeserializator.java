package root.deserializator;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import root.resources.entity.BigPokemon;
import java.io.IOException;

public class BigPokemonDeserializator extends StdDeserializer<BigPokemon> {

    public BigPokemonDeserializator() {
        this(null);
    }

    protected BigPokemonDeserializator(Class<?> vc) {
        super(vc);
    }

    @Override
    public BigPokemon deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        BigPokemon bigPokemon = new BigPokemon();

        int bigPokemonId = node.get("id").asInt();
        String name = node.get("name").asText();
        JsonNode abilities = node.get("abilities");

        bigPokemon.setBigPokemonId(bigPokemonId);
        bigPokemon.setName(name);
        bigPokemon.setAbilities(abilities);

        return bigPokemon;
    }
}