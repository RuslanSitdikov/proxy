package root.pokemonListReturner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import root.resources.entity.Pokemon;
import root.annotations.UseMock;
import root.gateway.PokemonGateway;
import java.util.List;

@Component
@UseMock(mockClass = PokemonListPlug.class)
public class PokemonMapper {

    @Autowired
    private PokemonGateway pokemonGateway;

    public List<Pokemon> getPokemonList(int offset) {
        List<Pokemon> pokemonList = pokemonGateway.getPokemonList(offset).getBody().getResults();
        pokemonList.stream().forEach(pokemon -> pokemon.setPokemonId(getPokemonIdFromUrl(pokemon)));
        return pokemonList;
    }

    public int getPokemonIdFromUrl(Pokemon pokemon) {
        return Integer.parseInt(pokemon.getUrl().split("/")[6]);
    }
}