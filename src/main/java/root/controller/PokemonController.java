package root.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import root.pokemonListReturner.PokemonMapper;
import root.service.PageNavigationService;
import ru.dexsys.task.service.*;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping()
@AllArgsConstructor
public class PokemonController {

    @Autowired
    PokemonMapper pokemonMapper;

    @Autowired
    private PageNavigationService pageNavigationService;

    @GetMapping("/pokemonList")
    public ModelAndView buttonIsPressed(HttpServletRequest request) {
        int offset;
        Optional<String> pressedButton = Optional.ofNullable(request.getParameter("button"));
        if (pressedButton.isPresent()) {
            offset = pressedButton.get().equals("Next") ? pageNavigationService.getNextOffset() : pageNavigationService.getPreviousOffset();
        } else {
            offset = 0;
        }
        return new ModelAndView(pageNavigationService.getCorrectJsp(), "PokemonList", pokemonMapper.getPokemonList(offset));
    }
}