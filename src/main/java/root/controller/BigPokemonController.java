package root.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import root.mapper.ResponceEntityToEntityResourceMapper;
import root.resources.entity.BigPokemonResource;

@RestController
@RequestMapping
@AllArgsConstructor
public class BigPokemonController {

    @GetMapping("/{pokemonId}")
    public BigPokemonResource drawBigPokemon(@PathVariable int pokemonId) {
        return new ResponceEntityToEntityResourceMapper().mapper(pokemonId);
    }
}
