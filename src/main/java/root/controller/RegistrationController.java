package root.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import root.service.UserDaoService;
import root.resources.entity.User;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/registration")
@AllArgsConstructor
public class RegistrationController {

    @Autowired
    UserDaoService userDaoService;

    @Autowired
    HttpServletRequest request;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping
    public ModelAndView getRegistrationPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("RegistrationPage");
        return modelAndView;
    }

    @PostMapping
    public String setUserLoginAndPassword() {
        User user = new User();
        user.setPassword(passwordEncoder.encode(request.getParameter("password")));
        user.setName(request.getParameter("login"));
        userDaoService.saveUserToDB(user);
        return "redirect:" + "http://localhost:8080/role";
    }
}
