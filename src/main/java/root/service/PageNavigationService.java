package root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;

@Service
public class PageNavigationService {

    @Autowired
    private HttpServletRequest request;

    public int getCurrentWebPageNumber() {
        return (int) Integer.parseInt(request.getParameter("pokemonId")) / 20;
    }

    public int getPreviousOffset() {
        return 20 * (getCurrentWebPageNumber() - 1);
    }

    public int getNextOffset() {
        return 20 * (getCurrentWebPageNumber() + 1);
    }

    public String getUserRole(){
        return (String) request.getSession().getAttribute("userRole");
    }

    public String getCorrectJsp(){
        String CorrectJsp;
        if (getUserRole().equals("Bokh")){
            CorrectJsp = "PokemonListBokh";
        } else {
            CorrectJsp = "PokemonListLokh";
        }
        return CorrectJsp;
    }

}