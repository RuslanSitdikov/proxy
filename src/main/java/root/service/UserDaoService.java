package root.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import root.repository.UserDao;
import root.resources.entity.User;

@Service
public class UserDaoService {

    @Autowired
    public UserDao userDao;

    public void saveUserToDB(User user){
        userDao.save(user);
    }
}
